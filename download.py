import os
import lakefs
from lakefs.client import Client

client = Client(
    username=os.environ.get("LAKECTL_CREDENTIALS_ACCESS_KEY_ID"),
    password=os.environ.get("LAKECTL_CREDENTIALS_SECRET_ACCESS_KEY"),
    host=os.environ.get("LAKECTL_URL"),
)

repo = lakefs.Repository(repository_id="homework", client=client)


def download(repo, branch, object_name, filename):
    br = repo.branch(branch)
    with (
        br.object(object_name).reader(mode="rb") as reader,
        open(filename, mode="wb") as writer,
    ):
        writer.write(reader.read())


download(repo, snakemake.params.branch, "data.csv", snakemake.output.dataset)
