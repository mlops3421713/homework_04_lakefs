from multiprocessing import cpu_count
from snakemake.report import auto_report


random_seed = 177013
test_size = 0.2
thread_count = lambda cores: max(1, cpu_count() // 2)

scaler_list = ['standard', 'boxcox']
model_list = ['lr', 'lda']
branch_list = ['production', 'staging']


rule all:
    input:
        metrics=expand('report/metrics/{model}_{scaler}_{branch}.png', model=model_list, scaler=scaler_list, branch=branch_list),
        cmatrix=expand('report/cmatrix/{model}_{scaler}_{branch}.png', model=model_list, scaler=scaler_list, branch=branch_list)

rule download:
    output:
        dataset='raw/data_{branch}.csv'
    params:
        branch='{branch}'
    script:
        "download.py"

rule preprocess:
    input:
        dataset='raw/data_{branch}.csv'
    output:
        train='processed/{branch}/{scaler}/train.csv',
        test='processed/{branch}/{scaler}/test.csv'
    params:
        random_seed=random_seed,
        test_size=test_size,
        scaler='{scaler}'
    threads:
        thread_count
    resources:
        mem_mib=64
    script:
        "preprocess.py"

rule train:
    input:
        train='processed/{branch}/{scaler}/train.csv',
    output:
        model='processed/{branch}/{scaler}/model_{model}.joblib',
        encoder='processed/{branch}/{scaler}/encoder_{model}.joblib'
    params:
        random_seed=random_seed,
        model='{model}'
    threads:
        thread_count
    resources:
        mem_mib=64
    script:
        "train.py"

rule evaluate:
    input:
        test='processed/{branch}/{scaler}/test.csv',
        model='processed/{branch}/{scaler}/model_{model}.joblib',
        encoder='processed/{branch}/{scaler}/encoder_{model}.joblib'
    output:
        metrics='report/metrics/{model}_{scaler}_{branch}.png',
        cmatrix='report/cmatrix/{model}_{scaler}_{branch}.png'
    threads:
        thread_count
    resources:
        mem_mib=64
    script:
        "evaluate.py"

onsuccess:
    with open("report/dag.txt","w") as f:
        f.writelines(str(workflow.persistence.dag))
    shell("cat report/dag.txt | dot -Tsvg > report/dag.svg")
