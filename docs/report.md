# Classification metrics

TL;DR: more data does not change the metrics significantly. Perhaps a more complex approach is due.

## Dataset v1 (less data)

### Logistic Regression (standard scaler)

![Curves](report/metrics/lr_standard_production.png)
![Matrix](report/cmatrix/lr_standard_production.png)

### Logistic Regression (power transformer)

![Curves](report/metrics/lr_boxcox_production.png)
![Matrix](report/cmatrix/lr_boxcox_production.png)

### Linear Discriminant (standard scaler)

![Curves](report/metrics/lda_standard_production.png)
![Matrix](report/cmatrix/lda_standard_production.png)

### Linear Discriminant (power transformer)

![Curves](report/metrics/lda_boxcox_production.png)
![Matrix](report/cmatrix/lda_boxcox_production.png)


## Dataset v2 (100% more data!)

### Logistic Regression (standard scaler)

![Curves](report/metrics/lr_standard_staging.png)
![Matrix](report/cmatrix/lr_standard_staging.png)

### Logistic Regression (power transformer)

![Curves](report/metrics/lr_boxcox_staging.png)
![Matrix](report/cmatrix/lr_boxcox_staging.png)

### Linear Discriminant (standard scaler)

![Curves](report/metrics/lda_standard_staging.png)
![Matrix](report/cmatrix/lda_standard_staging.png)

### Linear Discriminant (power transformer)

![Curves](report/metrics/lda_boxcox_staging.png)
![Matrix](report/cmatrix/lda_boxcox_staging.png)
